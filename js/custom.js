var globalDate = {};
globalDate.fixedDays = 1;
globalDate.typeNamesCount = 1;

function addFixedDays(){
	globalDate.fixedDays++;
	$('#fixedDates').append('<input id="fixedDate'+globalDate.fixedDays+'" name="fixedDates[]" class="datePicker span13" type="text">');
	$('#jsAddFixedDays').prepend('<div style="height:41px;">&nbsp;</div>');
	//$('#jsAddFixedDays').css({ "top":top_val});	
}

function addType(){
	globalDate.typeNamesCount++;
	for(var i = 1; i < globalDate.typeNamesCount; i++){
		$('#typeNameDiv'+i).removeClass('activeSubTab');
		$('#typeNameDiv'+i).addClass('inactiveSubTab');
	}
	var html = '<div id="typeNameDiv'+globalDate.typeNamesCount+'" class="span3 activeSubTab"><input id="typeName'+globalDate.typeNamesCount+'" name="typeName[]" class="span12" type="text" placeholder="Package/Type Name" /></div>';
	$('#jsAddTypeName').append(html);
	$('#typeNameDiv'+globalDate.typeNamesCount).focus();
}

$('#dateApplicability').on('change', function(){
	//alert($(this).val());
	var date_applicability = $(this).val();
	
	if(date_applicability == 2)
		$('#dateApplicabilitySpan').html('');
	else {
		if(date_applicability == 3){
			$('#dateApplicabilitySpan').html('<span class="BusDetCol7 officeOpenDay selectMultiBtns"><span><button>M</button></span><span><button>T</button></span><span><button>W</button></span><span><button>Th</button></span><span><button>F</button></span><span><button>S</button></span><span><button>Su</button></span></span>');
		}
		else{
			$('#dateApplicabilitySpan').html('<span id="fixedDaysSpan" class="BusDetCol3"><input type="text" id="fixedDate1" name="fixedDate[]" style="width:130px;"></span><span class="BusDetCol3"><a href="javascript:void(0);" onclick="javascript:addFixedDays();" class="CertainSize">+Add More Fixed Days</a></span>');
		}
	}

});