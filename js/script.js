/* Author:Yellow Slice Team
   www.yellowslice.in
   Contents:Custom Code For Pages
*/
/***************
	Smooth Scroll
***************/
$('section').each(function(){
  var $this = $(this),
      top = Math.floor($this.offset().top)
  ;
  $this.find('h2').append('<span>My top is '+ top +'px</span>');
});

$('.slideScroll').click(function(e){
  e.preventDefault();
  var top = (Math.floor($( this.getAttribute('href') ).offset().top));
  $('body,html').animate({ scrollTop: top },1500, function(){
  });
});
/***************
Home Page
***************/
var signInBoxOpen=false,
registerBoxOpen=false,
CloseHomeMainContent=false;
//Opening Sign In Box
function openSignInBox(){
	signInBoxOpen=true;
	if(registerBoxOpen==true){
		CloseHomeMainContent=true;
		closeRegisterBox();
		$('.signInContain').slideDown(1000);
	}
	else{
		CloseHomeMainContent=false;
		$('.homeBannerContain').animate({
			'margin-top':'700px'
		},500);
		$('.signInContain').slideDown(1000);
		$('#homeMainContent').fadeOut(1000);
	}
}
$('.signInBtn').click(function(e){
	e.preventDefault();
	openSignInBox();
	return false;
})
//closing Sign In Box
function closeSignInBox(){
	signInBoxOpen=false;
	$('.signInContain').slideUp(750);
	if(CloseHomeMainContent==false){
		$('#homeMainContent').fadeIn(100);
		$('.homeBannerContain').animate({
			'margin-top':'0'
		},1000)
	}

}
$('.signInCloseBtn').click(function(e){
	CloseHomeMainContent=false;
	closeSignInBox()
})
function openRegisterBox(){
	registerBoxOpen=true;
	if(signInBoxOpen==true){
		CloseHomeMainContent=true;
		closeSignInBox();
		$('.registerContain').slideDown(1000);
	}
	else{
		CloseHomeMainContent=false;
		$('.homeBannerContain').animate({
			'margin-top':'700px'
		},500);
		$('.registerContain').slideDown(1000);
		$('#homeMainContent').fadeOut(1000);
	}
}
//Opening Sign In Box
$('.registerBtn').click(function(e){
	e.preventDefault();
	openRegisterBox();
	return false;
})
//closing Sign In Box
function closeRegisterBox(){
	registerBoxOpen=false;
	$('.registerContain').slideUp(750);
	if(CloseHomeMainContent==false){
		$('#homeMainContent').fadeIn(100);
		$('.homeBannerContain').animate({
			'margin-top':'0'
		},1000)
	}
}
$('.joinCloseBtn').click(function(e){
	CloseHomeMainContent=false;
	closeRegisterBox();
})
$('.registerForm input[type=submit]').click(function(e){
	e.preventDefault();
	$.colorbox({width:"720px", height:"180px", inline:true, href:"#registerThanks"});
	return false ;
});
//function that will scroll to top of page
function scrollToTop(){
	$('body,html').animate({
		scrollTop: 0
	}, 800);
}

// About Help Content
$('[data-formAssistContentTop]').focus(function(){
	var $this=$(this);
	$('.formAssistContain').animate({
		top:$this.attr('data-formAssistContentTop')
	});
	$('.formAssistContent').html($(this).attr('data-formAssistContent'));
})
// About Help Content For chozn
/*$('[data-formAssistContentTop]').focus(function(){
	var $this=$(this);
	$('.formAssistContain').animate({
		top:$this.attr('data-formAssistContentTop')
	});
	$('.formAssistContent').html($(this).attr('data-formAssistContent'));
})*/

// $('.chosen-select').each(function(){
// 	$(this).focus(function(){
// 		alert("dsads")
// 	})
// });

	// $(this).focus(function(e){
	// 	alert("dsads")
 //    	var selectBox = $(this).closest('.chosen-container').prev('select');
 //    	console.log(selectBox)
 //    	console.log($(selectBox).attr('data-formAssistContent'))
 //    	$('.formAssistContain').animate({
 //    		top:$(selectBox).attr('data-formAssistContentTop')
 //    	});
 //    	$('.formAssistContent').html($(selectBox).attr('data-formAssistContent'));    
	// 	});
// Select mutiple buttons
$('.selectMultiBtns button').click(function(e){
	e.preventDefault();
	var $this = $(this);
	$this.toggleClass('currLink');
	if($this.hasClass('multiSelectAll')){
		$('.selectMultiBtns button').removeClass('currLink');
		$this.addClass('currLink')
	}
	else{
		if($('.multiSelectAll').hasClass('currLink')){
			$('.multiSelectAll').removeClass('currLink')
		}
	}
	return false
});


// About You
$('.youTab .youTabNextBtn').click(function(event){
	event.preventDefault();
	scrollToTop();
	$('.abtStatusBox1 .statusFull').animate({"height":"50%"});
	$('.youTab').fadeOut(function(){
			$('.yourDetTab').fadeIn();
	});
	$('.abtUAside').animate({
		'top':0
	},function(){
		$('.formAssistContent').html('<p>Tell us something about <strong>About Yourself!</strong></p>');
		$('.youTabBox').addClass('abtUStatusDone')
	});
	$('.yourDetTabBox').addClass('currLink');
	return false
});
$('.yourDetTabBackBtn').click(function(event){
	event.preventDefault();
	scrollToTop();
	$('.abtStatusBox1 .statusFull').animate({"height":"0"});
	$('.yourDetTab').fadeOut(function(){
			$('.youTab').fadeIn();
	});
	$('.abtUAside').animate({
		'top':0
	},function(){
		$('.formAssistContent').html('<p>Tell us something about <strong>About Yourself2!</strong></p>');
		$('.youTabBox').removeClass('abtUStatusDone')
	});
	$('.yourDetTabBox').removeClass('currLink');

	return false
});
// Your Business Next
$('.yourBusNext').click(function(event){
	event.preventDefault();
	scrollToTop();
	$('.abtStatusBox2 .statusFull').animate({"height":"25%"});
	$('.yourBusDetTab').fadeOut(function(){
			$('.yourBusStatutoryTab').fadeIn();
	});
	$('.abtUAside').animate({
		'top':0
	},function(){
		$('.formAssistContent').html('<p><strong>Statutory Details:</strong> Please input the relevant details. Also, upload a scanned copy of the PAN Card for our documentation Please note that only an Owner can access this section.</p>');
		$('.yourBusDetTabBox').addClass('abtUStatusDone');
	});
	$('.yourBusStatutoryTabBox').addClass('currLink');	

	return false
});
$('.yourBusStatNext').click(function(event){
	event.preventDefault();
	scrollToTop();
	$('.abtStatusBox2 .statusFull').animate({"height":"50%"});
	$('.yourBusStatutoryTab').fadeOut(function(){
			$('.yourBusBankTab').fadeIn();
	});
	$('.abtUAside').animate({
		'top':0
	},function(){
		$('.formAssistContent').html('<p><strong>Bank Details: </strong>Please fill in the bank details so that we can process all your payments in a safe manner. Please note that only an Owner can access this section</p>');
		$('.yourBusStatutoryTabBox').addClass('abtUStatusDone');
	});
	$('.yourBusBankTabBox').addClass('currLink');	

	return false
});

$('.yourBusBankNext').click(function(event){
	event.preventDefault();
	scrollToTop();
	$('.abtStatusBox2 .statusFull').animate({"height":"75%"});
	$('.yourBusBankTab').fadeOut(function(){
			$('.yourBusTeamTab').fadeIn();
	});
	$('.abtUAside').animate({
		'top':0
	},function(){
		$('.formAssistContent').html('<p><strong>Team Details:</strong> It is mandatory to include at least one team member to your business. Having multiple team members allows to select managers for different products. On completion of the form an email will be sent to the team member/s, inviting them to Login and update their settings.</p> ');
		$('.yourBusBankTabBox').addClass('abtUStatusDone');
	});
	$('.yourBusTeamTabBox').addClass('currLink');	

	return false
});

$('.yourBusTeamNext').click(function(event){
	// This will lead to Your Product page
});

//Go back
$('.yourBusStatutoryBack').click(function(event){
	event.preventDefault();
	scrollToTop();
	$('.abtStatusBox2 .statusFull').animate({"height":"0"});
	$('.yourBusStatutoryTab').fadeOut(function(){
			$('.yourBusDetTab').fadeIn();
	});
	$('.abtUAside').animate({
		'top':0
	},function(){
		$('.formAssistContent').html('<p>Tell us something <strong>About Your Business!</strong></p>');
		$('.yourBusDetTabBox').removeClass('abtUStatusDone');
	});
	$('.yourBusStatutoryTabBox').removeClass('currLink');	

	return false
});
$('.yourBusBankBack').click(function(event){
	event.preventDefault();
	scrollToTop();
	$('.abtStatusBox2 .statusFull').animate({"height":"50%"});
	$('.yourBusBankTab').fadeOut(function(){
			$('.yourBusStatutoryTab').fadeIn();
	});
	$('.abtUAside').animate({
		'top':0
	},function(){
		$('.formAssistContent').html('<p><strong>Statutory Details:</strong> Please input the relevant details. Also, upload a scanned copy of the PAN Card for our documentation Please note that only an Owner can access this section.</p>');
		$('.yourBusStatutoryTabBox').removeClass('abtUStatusDone');
	});
	$('.yourBusBankTabBox').removeClass('currLink');	

	return false
});

$('.yourBusTeamBack').click(function(event){
	event.preventDefault();
	scrollToTop();
	$('.abtStatusBox2 .statusFull').animate({"height":"75%"});
	$('.yourBusTeamTab').fadeOut(function(){
			$('.yourBusBankTab').fadeIn();
	});
	$('.abtUAside').animate({
		'top':0
	},function(){
		$('.formAssistContent').html('<p><strong>Bank Details: </strong>Please fill in the bank details so that we can process all your payments in a safe manner. Please note that only an Owner can access this section</p>');
		$('.yourBusBankTabBox').removeClass('abtUStatusDone');
	});
	$('.yourBusTeamTabBox').removeClass('currLink');	

	return false
});
//If All Access is Selected Disable others
$('#teamMemb1AllAccess').click(function(){
	var cbIsChecked = $(this).prop('checked');
	$('.BusDetCol2 table input[type=checkbox]').prop('disabled',cbIsChecked);	
	$('.BusDetCol2 table input[type=checkbox]').removeAttr('checked');	
})
$('.BusDetCol2 input[type=checkbox]').focus(function(){
	var $this = $(this);
	$('.formAssistContain').animate({
		top:$this.parent('[data-formAssistContentTop]').attr('data-formAssistContentTop')
	});
	$('.formAssistContent').html($this.parent('[data-formAssistContentTop]').attr('data-formAssistContent'));
});
$('.panCardDelete').click(function(e){
	e.preventDefault();
	$('.panCardYourBus').fadeOut();
	$('.panCardDelete').fadeOut();
	return false
});

