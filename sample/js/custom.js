var globalDate = {};
globalDate.fixedDays = 1;
globalDate.typeNamesCount = 1;
globalDate.allocSlabs = 4;

$(document).ready(function(){
	$('#typeName1').tooltip();
	$('#fixedDate1').datepicker();
});

function addFixedDays(){
	globalDate.fixedDays++;
	$('#fixedDates').append('<input id="fixedDate'+globalDate.fixedDays+'" name="fixedDates[]" class="datePicker span13" type="text">');
	$('#jsAddFixedDays').prepend('<div style="height:41px;">&nbsp;</div>');
	
	//$('#jsAddFixedDays').css({ "top":top_val});	
}

function addBlackOutDays(){
	var blackOutHTML = '<div class="row-fluid"><div class="span10"><div class="span4"></div><div class="span1">from</div><div class="span2"><input name="BODFrom[]" class="span12" id="" type="text" /></div><div class="span1">to</div><div class="span2"><input name="BODTo[]" class="span12" id="" type="text" /></div>';
	$('#blackOutDays').append(blackOutHTML);
}

function addAllocSlab(){

	var allocSlabHTML = '<div class="row-fluid" id="bbc_row"><div class="span2"><label for="Alloc"><strong>Slab '+globalDate.allocSlabs+'</strong></label></div><div class="span2"><input name="" class="span10" id="" type="text" /></div><div class="span2"><input name="" class="span10" id="" type="text" /></div><div class="span2"><input name="" class="span10" id="" type="text" /></div><div class="span2" id="bbc_row_dark"><label for="Alloc">8%</label></div><div class="span2" id="bbc_row_dark"><label for="Alloc">4%</label></div></div>';
	$('#allocSlabsContainer').append(allocSlabHTML);
	$(".chzn-select").chosen()
	globalDate.allocSlabs++;
}

function addRateSlab(){
	var addRateSlab = '<div class="row-fluid" id="bbc_row"><div class="span2"><label for="Alloc"><strong><input name="" class="span12" id="" type="text" /></strong></label></strong></label></div><div class="span2"><div class="span12"><div class="row-fluid"><div class="span6">Adult</div><div class="span6">Child</strong></div></div><div class="row-fluid"><div class="span6"><input name="" class="span6" id="" type="text" /></div><div class="span6"><input name="" class="span6" id="" type="text" /></strong></div></div></div></div><div class="span2"><input name="" class="span12" id="" type="text" /></div><div class="span1"><input name="" class="span12" id="" type="text" /></div><div class="span1"><input name="" class="span12" id="" type="text" /></div><div class="span2"><div class="span12"><div class="row-fluid"><div class="span4">All</div><div class="span8"><div class="row-fluid"><div class="span12"><input name="" class="span12" id="" type="text" /></div></div><div class="row-fluid"><div class="span12"><input name="" class="span12" id="" type="text" /></strong></div></div></div></div></div></div><div class="span2"><select name="dateApplicability" id="dateApplicability" class="chzn-select" tabindex="2" data-formAssistContentTop="50px" data-formAssistContent="<p>Allocation </p>" style="width:80px;"><option value="1">1</option><option value="2">2</option><option value="3">3</option></select></div></div>';

	$('#rateSlabContainer').append(addRateSlab);
	$(".chzn-select").chosen()
}
function addType(){
	globalDate.typeNamesCount++;
	for(var i = 1; i < globalDate.typeNamesCount; i++){
		$('#typeNameDiv'+i).removeClass('activeSubTab');
		$('#typeNameDiv'+i).addClass('inactiveSubTab');
	}
	var html = '<div id="typeNameDiv'+globalDate.typeNamesCount+'" class="span3 activeSubTab"><input id="typeName'+globalDate.typeNamesCount+'" name="typeName[]" class="span12" type="text" placeholder="Package/Type Name" /></div>';
	$('#jsAddTypeName').append(html);
	$('#typeNameDiv'+globalDate.typeNamesCount).focus();
}

$('#dateApplicability').on('change', function(){
	//alert($(this).val());
	var date_applicability = $(this).val();
	
	if(date_applicability == 2)
		$('#dateApplicabilitySpan').html('');
	else {
		if(date_applicability == 3){
			$('#dateApplicabilitySpan').html('<span class="BusDetCol7 officeOpenDay selectMultiBtns"><span><button>M</button></span><span><button>T</button></span><span><button>W</button></span><span><button>Th</button></span><span><button>F</button></span><span><button>S</button></span><span><button>Su</button></span></span>');
		}
		else{
			$('#dateApplicabilitySpan').html('<span id="fixedDaysSpan" class="BusDetCol3"><input type="text" id="fixedDate1" name="fixedDate[]" style="width:130px;"></span><span class="BusDetCol3"><a href="javascript:void(0);" onclick="javascript:addFixedDays();" class="CertainSize">+Add More Fixed Days</a></span>');
		}
	}

});
